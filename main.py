from Task2_Mat import mat_all, fib, array_prob, fib_rev, cumulative_sum


def run():
    mat_all()
    lst = fib(20)
    print(lst)
    fib_rev(lst)
    array_prob()
    list1 = [10, 20, 30, 40, 50]
    print(cumulative_sum(list1))


if __name__ == '__main__':
    run()
