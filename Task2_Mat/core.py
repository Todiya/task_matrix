import numpy
import random
import copy


def mat_all():
    matrix_one = numpy.ones((5, 5))
    print(matrix_one, '')

    random_matrix = numpy.random.randint(0, 100, (5, 5))
    print(random_matrix, '\n')

    arranged_matrix = numpy.arange(1, 10).reshape(3, 3)
    print(arranged_matrix, '\n')


def fib(n):
    if n == 0:
        return [0]
    elif n == 1:
        return [0, 1]
    else:
        lst = fib(n - 1)
        lst.append(lst[-1] + lst[-2])
    return lst


def fib_rev(lst):
    data = copy.copy(lst)
    random.shuffle(data)
    print(data)


def array_prob():
    for i in range(1, 100):
        print(numpy.asarray(i))


def cumulative_sum(x):
    l = len(x)
    s = []
    for i in range(1, l+1):
        s.append(sum(x[:i]))
    return s

