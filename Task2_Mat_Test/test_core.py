import random
import unittest
import numpy
from Task2_Mat import fib, fib_rev, cumulative_sum


class MyTestCase(unittest.TestCase):
    def test_mat_all(self):
        self.matrix1 = numpy.ones([5, 5])
        self.matrix2 = numpy.identity(5)
        numpy.allclose(self.matrix1, self.matrix2)

        i = random.randint(0, 100)
        self.assertLess(i, i + 1)
        self.assertLess(i - 1, i)

    def test_arranged(self):
        arr1 = numpy.array([(1, 2, 3), (4, 5, 6), (7, 8, 9)])
        arr2 = numpy.arange(1, 10).reshape(3, 3)
        (arr1 == arr2).all()

    def test_fib(self):
        wanted_result = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765]
        self.assertEqual(wanted_result, fib(20))

    def test_fib_rev(self):
        expected = [1, 144, 13, 1597, 89, 8, 233, 610, 2, 55, 1, 3, 2584, 987, 34, 4181, 377, 6765, 5, 21, 0]
        result = fib_rev(fib(20))
        self.assertTrue(expected, result)

    def test_cumulative_sum(self):
        x = [10, 20, 30, 40, 50]
        expected = [10, 30, 60, 100, 150]
        result = cumulative_sum(x)
        self.assertTrue(expected, result)


if __name__ == '__main__':
    unittest.main()
